from math import ceil,floor

dict_type_cpu_count = {'large'  : 1,'xlarge' : 2,'2xlarge' : 4,'4xlarge' : 8,'8xlarge' : 16,'10xlarge' : 32}

def get_costs(instances, hours, cpus = None, price = None):
	list_final_allocation = []
	for region in instances:
		for regionname,regioninstances in region.items():
			required_cpus=cpus
			list_region_instances = []
			list_allocated_instances={}
			total_cost=0
			for instancetype,instanceprice in regioninstances.items():
				dict_cpu_price = {'type':instancetype,'unitprice':instanceprice,'percpuprice':instanceprice/dict_type_cpu_count[instancetype],'cpucount':dict_type_cpu_count[instancetype]}
				list_region_instances.append(dict_cpu_price)
			
			if cpus!=None:
				list_region_instances_sorted_low_to_high = sorted(list_region_instances, key=lambda k: k['percpuprice']) 

				for instances_dict in list_region_instances_sorted_low_to_high:
					allocated_units=0
					if required_cpus>0 and instances_dict['cpucount']<=required_cpus:
						allocated_units=required_cpus/instances_dict['cpucount']
						allocated_cpus=allocated_units*instances_dict['cpucount']
						total_cost+=allocated_units*instances_dict['unitprice']
						list_allocated_instances[instances_dict['type']]=allocated_units
						required_cpus-=allocated_cpus
				
				list_region_instances_sorted_cpu_count = sorted(list_region_instances, key=lambda k: k['cpucount']) 
						
				if required_cpus>0 :
					for instances_dict in list_region_instances_sorted_cpu_count:
						allocated_units=ceil(required_cpus/instances_dict['cpucount'])
						if allocated_units==0: allocated_units=1
						allocated_cpus=allocated_units*instances_dict['cpucount']
						total_cost+=allocated_units*instances_dict['unitprice']	
						if instances_dict['type'] in list_allocated_instances:
							list_allocated_instances[instances_dict['type']]+=allocated_units
						else:
							list_allocated_instances[instances_dict['type']]=allocated_units
						required_cpus-=allocated_cpus
						if required_cpus<=0: break
				if price > 0 and round((total_cost*hours),2) > round(price,2) : continue
				dict_this_region={'region':regionname,'total_cost':'$' + str(round((total_cost*hours),2)),'servers':list_allocated_instances.items()}
				list_final_allocation.append(dict_this_region)
			if cpus==None and price>0:
				list_region_instances_sorted_low_to_high_unit_price = sorted(list_region_instances, key=lambda k: k['unitprice']) 
				lowestprice_cpu=list_region_instances_sorted_low_to_high_unit_price[0]
				allocated_units= floor(price/(lowestprice_cpu['unitprice']*hours))
				if allocated_units>0:
					
					list_allocated_instances[lowestprice_cpu['type']]=allocated_units
					dict_this_region={'region':regionname,'total_cost':'$' + str(round((allocated_units*lowestprice_cpu['unitprice']*hours),2)),'servers':list_allocated_instances.items()}
					list_final_allocation.append(dict_this_region)
	list_final_allocation = sorted(list_final_allocation,key=lambda k: k['total_cost'])
	return list_final_allocation
			
					
					
			
			
	

		
		
		
