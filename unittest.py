import cpucostcalc
dict_test_instances=[
		{"us-east": {
            "large": 0.12,
            "xlarge": 0.23,
            "2xlarge": 0.45,
            "4xlarge": 0.774,
            "8xlarge": 1.4,
            "10xlarge": 2.82
        }
        ,
        "us-west": {
            
            "4xlarge": 0.89,
            "10xlarge": 2.82,
            
            "2xlarge": 0.45
        }
        }
    ]
  
#case 1 : minimum cpu required
print cpucostcalc.get_costs(dict_test_instances,10,135)

output_case1 = [{'total_cost': '$120.0', 'region': 'us-east', 'servers': [('2xlarge', 1), ('large', 1), ('8xlarge', 8), ('xlarge', 1)]}, {'total_cost': '$121.8', 'region': 'us-west', 'servers': [('2xlarge', 2), ('10xlarge', 4)]}]

#case 2: fixed budget but give maximum possible machines for H hours
print cpucostcalc.get_costs(dict_test_instances,10,price=120)

output_case2 = [{'total_cost': '$117.0', 'region': 'us-west', 'servers': [('2xlarge', 26.0)]}, {'total_cost': '$120.0', 'region': 'us-east', 'servers': [('large', 100.0)]}]

#case 3: minimum cpu with fixed budget
print cpucostcalc.get_costs(dict_test_instances,10,135,120)

output_case3 = [{'total_cost': '$120.0', 'region': 'us-east', 'servers': [('2xlarge', 1), ('large', 1), ('8xlarge', 8), ('xlarge', 1)]}] 
    
